# FROM ruby:2.7.2-buster
# RUN apt-get -y update && apt-get -y install --no-install-recommends \
# cmake \
# git \
# libicu-dev \
# && rm -rf /var/lib/apt/lists/*


FROM ruby:2.7.2-alpine3.12
RUN apk add --no-cache \
bash \
build-base \
cmake \
git \
icu-dev \
openssl-dev \
shadow \
&& rm -rf /var/cache/apk/*


COPY Gemfile /root/
# RUN gem install \
# asciidoctor \
# creole  \
# github-linguist \
# gollum \
# org-ruby
RUN gem install -g /root/Gemfile

RUN mkdir -p /var/lib/gollum/wiki \
&& groupadd -g 1000 gollum \
&& useradd -d /var/lib/gollum -g gollum -u 1000 gollum \
&& chown -R gollum:gollum /var/lib/gollum

USER gollum

RUN git init /var/lib/gollum/wiki

WORKDIR /var/lib/gollum/wiki

ARG username=Anonymous
ARG useremail=anon@anon.com
RUN git config --local user.name "${username}" \
&& git config --local user.email "${useremail}"

ENTRYPOINT ["gollum"]
CMD ["--base-path=/var/lib/gollum/wiki"]

EXPOSE 4567

VOLUME ["/var/lib/gollum/wiki"]

