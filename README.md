# gollum-personal-wiki

This project uses [Gollum](https://github.com/gollum/gollum) running in a Docker container to provide a personal wiki.

## Building the container

By default, commits are associated with Anonymous <anon@anon.com>
The following build arguments can passed to `docker build` to change the defaults:

* username
* useremail

